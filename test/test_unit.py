import io
import os
import sys
from copy import copy
from contextlib import contextmanager
from unittest import TestCase

import pytest

from pipe.pipe import EmailNotify, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class EmailNotifyTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker, requests_mock):
        self.caplog = caplog
        self.mocker = mocker
        self.request_mock = requests_mock

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_default_auth_success(self):
        self.mocker.patch.dict(
            os.environ, {
                'USERNAME': 'myemail@example.com',
                'PASSWORD': 'password',
                'FROM': 'myemail@example.com',
                'TO': 'example@example.com',
                'HOST': 'smtp.gmail.com',
            }
        )

        pipe = EmailNotify(
            schema=schema, check_for_newer_version=True)

        auth_method = pipe.discover_auth_method()

        self.assertRegex(auth_method, EmailNotify.DEFAULT_AUTH)

    def test_no_auth_success(self):
        self.mocker.patch.dict(
            os.environ, {
                'USERNAME': 'myemail@example.com',
                'PASSWORD': 'password',
                'FROM': 'myemail@example.com',
                'TO': 'example@example.com',
                'HOST': 'smtp.gmail.com',
                'DISABLE_AUTH': 'true'
            }
        )

        pipe = EmailNotify(
            schema=schema, check_for_newer_version=True)

        auth_method = pipe.discover_auth_method()

        self.assertRegex(auth_method, EmailNotify.NO_AUTH)

    def test_no_auth_non_boolean_fails(self):
        self.mocker.patch.dict(
            os.environ, {
                'USERNAME': 'myemail@example.com',
                'PASSWORD': 'password',
                'FROM': 'myemail@example.com',
                'TO': 'example@example.com',
                'HOST': 'smtp.gmail.com',
                'DISABLE_AUTH': 'non-boolean-value',
            }
        )

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:

                EmailNotify(schema=schema, check_for_newer_version=True)

        assert "DISABLE_AUTH:\n- must be of boolean type" in out.getvalue()
        assert pytest_wrapped_e.type is SystemExit
