import os
import imaplib
import email
import base64
from random import randint
import uuid

import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit.helpers import required, get_variable


class EmailStructureException(Exception):
    pass


class MessageNotFoundInMailbox(Exception):
    pass


IMAP_HOST = 'imap.gmail.com'


class EmailNotifyBaseTestCase(PipeTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # get Variables
        cls.USERNAME = required('USERNAME')
        cls.PASSWORD = required('PASSWORD')
        cls.FROM = required('FROM')
        cls.TO = required('TO')
        cls.HOST = get_variable('HOST')
        cls.DEBUG = get_variable('DEBUG')

        cls.MAILBOX = imaplib.IMAP4_SSL(IMAP_HOST)
        cls.MAILBOX.login(cls.USERNAME, cls.PASSWORD)

    @classmethod
    def tearDownClass(cls):
        cls.MAILBOX.logout()

    def setUp(self):
        self.cwd = os.getcwd()
        self.api_client = self.docker_client.api
        self.random_number = randint(0, 1000000)
        self.TEST_SUBJECT = f'TEST:{self.random_number}'
        self.TEST_BODY_PLAIN = f'TEST:BODY:{self.random_number}'


class EmailNotifyTestCase(EmailNotifyBaseTestCase):

    @pytest.mark.run(order=1)
    def test_no_parameters(self):
        result = self.run_container()

        self.assertIn('USERNAME:\n- required field', result)

    def test_fail_login(self):
        result = self.run_container(
            environment={
                'USERNAME': 'fail_user_name@example.com',
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': self.TEST_SUBJECT,
                'DEBUG': 'true',
            }
        )

        # TODO mv messages to config file
        self.assertIn('Check your configuration settings', result)

    def test_default_subject(self):
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
            })

        self.assertIn('The mail has been sent successfully', result)

    def test_bcc_emails(self):
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'BCC': get_variable("BCC"),
                'HOST': self.HOST,
            })

        self.assertIn('The mail has been sent successfully', result)

    def test_fail_wrong_port(self):
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
                'PORT': 250001,
                'SUBJECT': self.TEST_SUBJECT,
                'DEBUG': 'true',
            }
        )

        # TODO mv messages to config file
        self.assertIn('Cannot assign requested address', result)

    def test_fail_wrong_tls(self):
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
                'PORT': 587,
                'TLS': 'false',
                'SUBJECT': self.TEST_SUBJECT,
                'DEBUG': 'true',
            }
        )

        # TODO mv messages to config file
        self.assertIn('Check your configuration settings', result)

    @pytest.mark.run(order=2)
    def test_default_success(self):
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': self.TEST_SUBJECT,
            })

        self.assertIn('The mail has been sent successfully', result)
        self.assertIn(
            self.TEST_SUBJECT,
            get_email_subject(self.MAILBOX, self.TEST_SUBJECT))

    @pytest.mark.skip(reason="AWS SES account deleted.")
    @pytest.mark.skipif(
        get_variable('AWS_ACCESS_KEY_ID') is None,
        reason="AWS_ACCESS_KEY_ID not provided")
    def test_default_success_aws_ses(self):
        USERNAME = required('AWS_ACCESS_KEY_ID')
        PASSWORD = required('AWS_SECRET_ACCESS_KEY')
        HOST = 'email-smtp.us-east-1.amazonaws.com'

        subject = f'AWSSES:{self.TEST_SUBJECT}'

        result = self.run_container(
            environment={
                'USERNAME': USERNAME,
                'PASSWORD': PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': HOST,
                'SUBJECT': subject,
            })

        self.assertIn('The mail has been sent successfully', result)

    @pytest.mark.skip(reason="Outlook locked the test account")
    def test_default_success_msoutlook(self):
        # note! MS OUTLOOK has The daily message limit for new accounts
        # TODO need test account MS Outlook with lage limit
        USERNAME = required('MS_OUTLOOK_USERNAME')
        PASSWORD = required('MS_OUTLOOK_PASSWORD')
        HOST = 'smtp.office365.com'

        subject = f'MSOUTLOOK:{self.TEST_SUBJECT}'

        result = self.run_container(
            environment={
                'USERNAME': USERNAME,
                'PASSWORD': PASSWORD,
                'FROM': USERNAME,
                'TO': self.TO,
                'HOST': HOST,
                'SUBJECT': subject,
            })

        self.assertIn('The mail has been sent successfully', result)

    @pytest.mark.run(order=3)
    def test_success_body_plain_text(self):
        self.TEST_BODY_PLAIN = f'TEST:BODY:{self.random_number}'

        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': self.TEST_SUBJECT,
                'BODY_PLAIN': self.TEST_BODY_PLAIN,
            })

        self.assertIn('The mail has been sent successfully', result)
        self.assertEqual(
            self.TEST_BODY_PLAIN,
            get_email_body(self.MAILBOX, self.TEST_BODY_PLAIN))

    def test_success_body_html(self):
        self.TEST_HTML_BODY = """
            <html>
                <head></head>
                <body>
                    <h1>{}</h1>
                    <h1>Hello Tempfile</h1>
                    <img src="https://www.atlassian.com/assets/img/icons/imkt/imkt-navbar__charlie-logo.svg" alt="Atlassian logo">
                    <p>Email sent from <a href='https://bitbucket.org'>We can do it - build link.</a></p>
                </body>
            </html>""".format(self.TEST_BODY_PLAIN)

        filename = 'test_html_body'

        with open(filename, 'w+') as fp:
            fp.write(self.TEST_HTML_BODY)

        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.FROM,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': self.TEST_SUBJECT,
                'BODY_HTML': filename,
            }
        )
        self.assertIn('The mail has been sent successfully', result)
        self.assertIn(
            self.TEST_BODY_PLAIN,
            get_email_body(self.MAILBOX, self.TEST_BODY_PLAIN, 'html'))

    def test_sending_attachments(self):
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.USERNAME,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': 'Test sending attachments',
            })

        self.assertIn('The mail has been sent successfully', result)


class EmailNotifyAttachmentsTestCase(EmailNotifyBaseTestCase):
    def setUp(self):
        super().setUp()
        self.attachments = [os.path.join(os.getcwd(), str(uuid.uuid4())) for i in range(3)]
        for attachment in self.attachments:
            with open(attachment, 'w+') as f:
                f.write(attachment)

    def tearDown(self):
        for attachment in self.attachments:
            os.remove(attachment)

    def test_attachment_is_not_exist(self):
        subject = 'Test sending attachments not_exist.txt'
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.USERNAME,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': subject,
                'ATTACHMENTS': 'not_exist.txt'
            })

        self.assertIn('Failed to add an attachment. No such file', result)

    def test_attachment_is_present(self):
        subject = f'Test sending attachments {self.attachments[0]}'
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.USERNAME,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': subject,
                'ATTACHMENTS': ','.join(self.attachments[:1])
            })

        self.assertIn('The mail has been sent successfully', result)
        attachments = get_email_attachments(self.MAILBOX, self.attachments[0])
        self.assertTrue(attachments)

    def test_multiple_attachments_are_present(self):
        subject = f'Test sending attachments {self.attachments[0]}'
        result = self.run_container(
            environment={
                'USERNAME': self.USERNAME,
                'PASSWORD': self.PASSWORD,
                'FROM': self.USERNAME,
                'TO': self.TO,
                'HOST': self.HOST,
                'SUBJECT': subject,
                'ATTACHMENTS': ','.join(self.attachments)
            })

        self.assertIn('The mail has been sent successfully', result)
        attachments = get_email_attachments(self.MAILBOX, self.attachments[0])
        self.assertEqual(len(attachments), 3)


def get_email_attachments(mailbox, text):
    attachments = []

    data = get_email_data(mailbox, text, 'SUBJECT')
    msg = email.message_from_bytes(data[0][1])

    if msg.get_content_maintype() != 'multipart':
        return []

    for part in msg.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue

        filename = part.get_filename()
        if filename is not None:
            attachments.append(part.get_payload(decode=True))
    return attachments


def get_email_subject(MAILBOX, text):
    data = get_email_data(MAILBOX, text, 'SUBJECT')
    msg = email.message_from_bytes(data[0][1])

    return str(email.header.make_header(
        email.header.decode_header(msg['Subject'])))


def get_email_body(MAILBOX, text, mime_type='plain'):
    data = get_email_data(MAILBOX, text, 'BODY')
    msg = email.message_from_bytes(data[0][1])

    payloads = msg.get_payload()
    if len(payloads) != 2:
        raise EmailStructureException(
            'Email should have txt and html part')
    part1 = payloads[0].get_payload()
    part2 = payloads[1].get_payload()

    text = base64.urlsafe_b64decode(part1).decode('utf-8')
    html = base64.urlsafe_b64decode(part2).decode('utf-8')

    return html if mime_type == 'html' else text


def get_email_data(MAILBOX, text, by_email_item):
    # select a mailbox
    MAILBOX.select()
    typ, msgnums = MAILBOX.search(None, by_email_item, text)
    try:
        typ, data = MAILBOX.fetch(msgnums[0], '(RFC822)')
    except Exception:
        raise MessageNotFoundInMailbox(f"text {text} in {by_email_item}")

    return data
