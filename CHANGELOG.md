# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.13.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.13.1

- patch: Fix support for DISABLE_AUTH variable.

## 0.13.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.12.0

- minor: Add support for the DISABLE_AUTH variable, a flag to disable username/password basic authorization. Provide the possibility to connect to an SMTP-Server that allows IP-based authentication instead of username/password.

## 0.11.0

- minor: Add support for BCC variable for mass emails.

## 0.10.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.9.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit version to 4.0.0. Update packages in test/requirements.txt.
- minor: Internal maintenance: Update docker image in Dockerfile to python:3.10-slim.
- patch: Internal maintenance: Update docker image and pipes versions in pipelines configuration file.

## 0.8.0

- minor: Make variables USERNAME, PASSWORD, FROM, TO, HOST as a default parameters.
- patch: Internal maintenance: update release process.

## 0.7.0

- minor: Update README examples. Add information that gmail is supported with google app password.

## 0.6.0

- minor: Update README examples. Add information that gmail is not supported for less secure apps.
- patch: Internal maintenance: Bump bitbucket-pipe-release version.
- patch: Internal maintenance: Skip tests until refactor to Amazon SES will be made.

## 0.5.0

- minor: Update README documentation with example of sending email to multiple recipients.
- patch: Internal maintenance: Bump version of bitbucket-pipes-toolkit to 3.2.1

## 0.4.5

- patch: Internal maintenance: Bump dependency bitbucket-pipes-toolkit to 2.2.0 version.

## 0.4.4

- patch: Fix and add example sending notification with a build status.

## 0.4.3

- patch: Internal maintenance: refactor tests to use conventional aws creds.

## 0.4.2

- patch: Internal maintenance: fix tests.

## 0.4.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.4.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.3.12

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.11

- patch: Internal maintenance: Fix test infrastructure.

## 0.3.10

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.9

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.8

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.7

- patch: Add warning message about new version of the pipe available.

## 0.3.6

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.3.5

- patch: Added code style checks

## 0.3.4

- patch: Internal maintenance: Fix issue. Update Readme.

## 0.3.3

- patch: Internal maintenance: update pipes toolkit version

## 0.3.2

- patch: Documentation updates

## 0.3.1

- patch: Fix the bug with a default SUBJECT missing

## 0.3.0

- minor: Introduced the support for sending email notifications with attachments

## 0.2.1

- patch: Updated the internal dependency version

## 0.2.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.1.0

- minor: Initial release
